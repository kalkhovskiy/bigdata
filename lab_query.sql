use shedule_db
-- Количество занятий по преподавателям
select t.rank, t.title, count(s.id) 
  from teacher t
  join schedule s on t.id = s.teacher_id 
 group by t.rank, t.title
 order by title;
 
--Какие предметы ведут чаще всего
with dc as (
select s.teacher_id, s.discipline_id, count(s.id) dis_count 
  from schedule s
 group by s.teacher_id, s.discipline_id
)
select t.rank, t.title, d.title, s.dis_count
  from teacher t 
  join dc s on s.teacher_id = t.id
  join discipline d on d.id = s.discipline_id
 where s.dis_count = (select max(dis_count) from dc s2 where s2.teacher_id = s.teacher_id)
order by t.title, d.title;
  
--В каких аудиториях ведут чаще всего
with dc as (
select ps.teacher_id, ps.auditorium_id, count(ps.id) dis_count 
  from public.schedule ps
 group by ps.teacher_id, ps.auditorium_id
)
select t.rank, t.title, d.title, s.dis_count
  from public.teacher t 
  join dc s on s.teacher_id = t.id
  join public.auditorium d on d.id = s.auditorium_id
 where s.dis_count = (select max(dis_count) from dc s2 where s2.teacher_id = s.teacher_id)
order by t.title, d.title;


--Количество пар преподавателя, разделенное по типам занятий
select t.rank, t.title, s.kind_of_work, count(s.id)
  from public.teacher t 
  join public.schedule s on s.teacher_id = t.id
 group by t.rank, t.title, s.kind_of_work
 order by t.title, s.kind_of_work;

