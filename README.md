# Лабораторная работа №4
## Альховский Константин МО-201, ОмГТУ

Для выполнения лабораторной работы была создана база данных _shedule_db_ и в ней были созданы таблицы при помощи скрипта на языке _sql_, который находится в файле _Create_db.sql_

В результате были созданы таблицы:

![Таблицы БД](img/db.jpg)

Схема получившейся БД:

![Схема БД](img/scheme_db.jpg)

Далее БД была заполнена при помощи скрипта на языке _python_, который находится в файле _db_download.py_

В результате таблицы имеют следующее содержание:

- teachers:

![teachers_table](img/teachers.jpg)

- discipline:

![discipline_table](img/discipline.jpg)

- group:

![group_table](img/group.jpg)

- class:

![class_table](img/class.jpg)

- slots:

![slots_table](img/slots.jpg)

- shedule:

![shedule_table](img/shedule.jpg)

Далее, для выполнения необходимых вычислений был написан запрос на языке _sql_, который находится в файле _lab_query.sql_

Результат выполнения данного запроса: 

![Результат запроса](img/query_result.jpg)