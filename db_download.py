from datetime import datetime
import json
import os

# подключаемся к базе
import pyodbc

server = 'DESKTOP-MFT0I2A\SQLEXPRESS'
database = 'shedule_db'
username = 'python_script'
password = '1'

conn_str = f'DRIVER={{SQL Server}};SERVER={server};DATABASE={database};UID={username};PWD={password}'
conn = pyodbc.connect(conn_str)

cursor = conn.cursor()
cursor.execute('use shedule_db')

####################

def save_group(group):
    with conn.cursor() as cursor:
        cursor.execute('INSERT INTO "group" (id, title) VALUES (%s, %s)', (group['id'], group['label'],))
    conn.commit()

def get_auditorium_id(schedule_day):
    auditorium_id = schedule_day['auditoriumOid']
    with conn.cursor() as cursor:
        ret = cursor.execute("SELECT id FROM auditorium WHERE id = ?", (auditorium_id))
        res = cursor.fetchone()
        if not res:
            cursor.execute("INSERT INTO auditorium (id, title, building, amount) VALUES (?, ?, ?, ?)",
                            (auditorium_id,
                            schedule_day['auditorium'],
                            schedule_day['building'],
                            schedule_day['auditoriumAmount'],))
    conn.commit()
    return auditorium_id

def get_lecturer_id(schedule_day):
    lecturer_id = schedule_day['lecturerOid']
    with conn.cursor() as cursor:
        ret = cursor.execute("SELECT id FROM teacher WHERE id = ?", (lecturer_id,))
        res = cursor.fetchone()
        if not res:
            cursor.execute("INSERT INTO teacher (id, title, rank) VALUES (?, ?, ?)",
                            (lecturer_id,
                            schedule_day['lecturer_title'],
                            schedule_day['lecturer_rank'],))
    conn.commit()
    return lecturer_id

def get_discipline_id(schedule_day):
    discipline_id = schedule_day['disciplineOid']
    with conn.cursor() as cursor:
        ret = cursor.execute("SELECT id FROM discipline WHERE id = ?", (discipline_id,))
        res = cursor.fetchone()
        if not res:
            cursor.execute("INSERT INTO discipline (id, title) VALUES (?, ?)",
                            (discipline_id,
                            schedule_day['discipline'],))
    conn.commit()
    return discipline_id

def get_slot_id(schedule_day):
    
    with conn.cursor() as cursor:
        slot_date = datetime.strptime(schedule_day['date'], '%Y.%m.%d').date()
        slot_start = datetime.strptime(schedule_day['beginLesson'], '%H:%M').time()
        slot_end = datetime.strptime(schedule_day['endLesson'], '%H:%M').time()
        ret = cursor.execute('SELECT id FROM slot WHERE date = ?  and time_start =  ? and time_end =  ?', (str(slot_date), str(slot_start), str(slot_end)))
        slot_id = cursor.fetchone()
        if slot_id:
            slot_id = slot_id[0]
        else:
            cursor.execute("INSERT INTO slot (date, time_start, time_end) VALUES (?, ?, ?)",
                            (str(slot_date), str(slot_start), str(slot_end)))
            
            cursor.execute("SELECT @@IDENTITY AS 'ID'")
            row = cursor.fetchone()
            slot_id = row.ID
    conn.commit()
    return slot_id

def get_group_id(schedule_day):
    group_id = schedule_day['groupOid']
    if group_id != 0:
        with conn.cursor() as cursor:
            ret = cursor.execute('SELECT id FROM "group" WHERE id = ?', (group_id,))
            res = cursor.fetchone()
            if not res:
                cursor.execute('INSERT INTO "group" (id, title) VALUES (?, ?)',
                                (group_id,
                                schedule_day['group'],))
        conn.commit()

    else:
        group_id = schedule_day['subGroupOid']
        with conn.cursor() as cursor:
            ret = cursor.execute('SELECT id FROM "group" WHERE id = ?', (group_id,))
            res = cursor.fetchone()
            if not res:
                cursor.execute('INSERT INTO "group" (id, title) VALUES (?, ?)',
                                (group_id,
                                schedule_day['subGroup'],))
        conn.commit()
    return group_id

def group_schedule_save(schedule_day):
    with conn.cursor() as cursor:
        auditorium_id = get_auditorium_id(schedule_day),
        group_id = get_group_id(schedule_day),
        lecturer_id = get_lecturer_id(schedule_day),
        discipline_id = get_discipline_id(schedule_day),
        slot_id = get_slot_id(schedule_day)
        #print(auditorium_id, group_id, lecturer_id, discipline_id, discipline_id, slot_id)
        cursor.execute('INSERT INTO schedule(auditorium_id, group_id, teacher_id, discipline_id, slot_id, kind_of_work) VALUES(?, ?, ?, ?, ?, ?)''', 
                       (auditorium_id[0],
                        group_id[0],
                        lecturer_id[0],
                        discipline_id[0],
                        slot_id,
                        schedule_day['kindOfWork'],))
    conn.commit()


#with open(r'D:\bigDataLr2\lr4\782898-2023.08.28-2023.12.31.json', 'r', encoding='utf-8') as file:
#    data = json.load(file)
#
#for schedule_day in data:
#    group_schedule_save(schedule_day)

#with open(r'D:\bigDataLr2\lr4\1003026-2023.08.28-2023.12.31.json', 'r', encoding='utf-8') as file:
#    data = json.load(file)
#
#for schedule_day in data:
#    group_schedule_save(schedule_day)


dataPath = r'D:\bigDataLr2\lr4\data\\'
for root, dirs, files in os.walk(dataPath):
  for file in files:
    with open(dataPath + file, 'r', encoding='utf-8') as file:
        data = json.load(file)
    for schedule_day in data:
        group_schedule_save(schedule_day)