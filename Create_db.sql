use shedule_db

--DROP TABLE "group"

CREATE TABLE "group" (
	id int NOT NULL ,
	title varchar(50) NOT NULL, -- Название группы
	amount int NULL, -- Численность
	CONSTRAINT group_pk PRIMARY KEY (id)
);

--DROP TABLE teacher

CREATE TABLE teacher (
	id int NOT NULL ,
	title varchar(150) NOT NULL, -- ФИО
	"rank" varchar(50) NULL, -- должность
	CONSTRAINT teacher_pk PRIMARY KEY (id)
);

--DROP TABLE auditorium;

CREATE TABLE auditorium (
	id int NOT NULL ,
	title varchar(100) NOT NULL, -- Название
	building varchar(50) NULL, -- Корпус
	"class" varchar(50) NULL, -- Тип аудитории
	amount int NULL, -- Вместимость
	CONSTRAINT auditorium_pk PRIMARY KEY (id)
);

--DROP TABLE discipline;

CREATE TABLE discipline (
	id int NOT NULL,
	title varchar(150) NOT NULL, -- Название
	CONSTRAINT discipline_pk PRIMARY KEY (id)
);

--DROP TABLE slot;

CREATE TABLE slot (
	id int IDENTITY(1,1),
	"date" date NOT NULL, -- Дата
	time_start time NOT NULL, -- Время начала
	time_end time NOT NULL, -- Время окончания
	"number" int NULL, -- Номер пары
	CONSTRAINT slot_pk PRIMARY KEY (id)
);

--DROP TABLE schedule;

CREATE TABLE schedule (
	id int IDENTITY(1,1),
	auditorium_id int NOT NULL, -- Ссылка на аудиторию
	group_id int NOT NULL, -- Группа
	teacher_id int NOT NULL, -- Преподаватель
	discipline_id int NOT NULL, -- Дисциплина
	slot_id int NULL, -- Слот
	create_date timestamp NULL, -- Дата и время создания
	kind_of_work varchar(50) null, -- Вид занятий - практика/лекция и т.п.
	CONSTRAINT schedule_pk PRIMARY KEY (id),
	CONSTRAINT schedule_un UNIQUE (auditorium_id, group_id, teacher_id, discipline_id, slot_id)
);

ALTER TABLE schedule ADD CONSTRAINT schedule_fk FOREIGN KEY (auditorium_id) REFERENCES auditorium(id);
ALTER TABLE schedule ADD CONSTRAINT schedule_fk_1 FOREIGN KEY (group_id) REFERENCES "group"(id);
ALTER TABLE schedule ADD CONSTRAINT schedule_fk_2 FOREIGN KEY (teacher_id) REFERENCES teacher(id);
ALTER TABLE schedule ADD CONSTRAINT schedule_fk_3 FOREIGN KEY (discipline_id) REFERENCES discipline(id);
ALTER TABLE schedule ADD CONSTRAINT schedule_fk_4 FOREIGN KEY (slot_id) REFERENCES slot(id);

INSERT INTO "group"
(id, title, amount)
VALUES(0, 'all', 0);
